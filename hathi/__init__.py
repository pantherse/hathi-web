# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

from flask import Flask, session

from flask_login import LoginManager


def create_app(test_config=None):
    app = Flask(__name__)
    if test_config is None:
        if 'HATHI_CONFIG' in os.environ:
            app.config.from_json(os.environ['HATHI_CONFIG'])
        else:
            app.config.from_json('config/server.cfg')
    else:
        app.config.from_mapping(test_config)

    from logging.config import fileConfig
    fileConfig(app.config['LOG_CONFIG'])

    logger = app.logger
    logger.info('Starting app')
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    login = LoginManager()
    login.init_app(app)
    login.login_view = "front.index"

    @login.user_loader
    def load_user(sess_id):
        from . import user
        logger.debug("Value of sess_id: %s", sess_id)

        ret_val = None
        try:
            actor = session['actor']
            logger.debug("Value of actor: %s", actor)
            ret_val = user.User().get_user(sess_id, actor)
        except KeyError as err_obj:
            if 'actor' in err_obj:
                logger.info("'actor' not in session")
            else:
                logger.error("Unexpected KeyError on %s", str(err_obj))
        except BaseException as err_obj:
            logger.error(
                "Exception encountered verifying session with the server. Cause: %s",
                str(err_obj))

        return ret_val

    from . import front
    app.register_blueprint(front.bp)

    from . import note
    app.register_blueprint(note.bp)

    from . import actor
    app.register_blueprint(actor.bp)

    return app

# vim: set expandtab:ts=4:sw=4
