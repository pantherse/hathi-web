# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from activipy import core

from flask import (
    Blueprint, flash, render_template, request
)

from flask_login import login_required


from .utils import getObject

bp = Blueprint("actor", __name__, url_prefix="/actor")


def get_name(obj):
    logger = logging.getLogger("Actor")
    logging.debug("Get value of name")

    if "Person" not in obj.types:
        logger.error("Provided object is not a \"Person\" object")
        raise TypeError("\"Person\" object expected")

    try:
        retVal = obj["name"]
        if isinstance(retVal, dict):
            try:
                retVal = retVal["en"]
            except KeyError:
                logger.info("\"name\" dictionary doesn't specify \"en\" "
                            "string to use. Pick the 1st one on the list")
                for key, val in retVal.items():
                    retVal = val
                    break
    except KeyError:
        logger.info("\"name\" member not in Actor object. Using ID for name")
        retVal = obj.id

    logger.debug("Name to return: %s", retVal)
    return retVal


@bp.route("/", methods=["GET"])
@login_required
def index():
    logger = logging.getLogger("Actor")

    template = "actor/actorview.html"
    name = None
    try:
        idVal = request.args["id"]
        logger.info("Get actor \"%s\" info", idVal)
        data = getObject(idVal)

        if data is None:
            flash("Actor \"%s\" doesn't exist" % (idVal), "warning")
        else:
            actor = core.ASObj(data)
            name = get_name(actor)
            logger.debug("Actor's name: \"%s\"", name)
    except KeyError as e:
        if e.args == "name":
            name = "Not available"
        else:
            logger.exception("Key error encountered while retrieving "
                             "actor info")
            flash("Error encountered while retrieving actor info", "error")

    return render_template(template, name=name)

# vim: set expandtab:ts=4:sw=4
