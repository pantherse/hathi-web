# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Hathi protocol handling"""

import socket
import json
import logging

from flask import session

class ProtocolException(RuntimeError):
    """Exception for issues related to errors returned (mostly) by the server
    when processing functions"""
    def __init__(self, message):
        RuntimeError.__init__(self)
        self.message = message

    def __str__(self):
        return self.message

class Protocol:
    """Hathi protocol implementation"""

    # pylint: disable=too-few-public-methods

    def __init__(self, host, port, timeout=None):
        """Initialize class"""

        self._logger = logging.getLogger("Protocol")
        self._expect_request = None
        try:
            self._conn = socket.create_connection((host, port), timeout)
        except socket.timeout:
            self._logger.info("Connect to server timed-out")
            raise

    def __del__(self):
        if self._conn:
            self._logger.debug("Closing remote connection")
            try:
                self._conn.shutdown(socket.SHUT_RDWR)
                self._conn.close()
            except BaseException as err_obj: # pylint: disable=broad-except
                self._logger.info("Error encountered during disconnect: %s",
                                  str(err_obj))

    def _send_msg(self, obj):
        """Send JSON message

        This adds the other required items needed to be included in the message.
        """

        try:
            self._expect_request = obj['request'] = session['protocol_request']
        except KeyError:
            self._logger.debug("Initialize protocol request counter")
            self._expect_request = obj['request'] = session['protocol_request'] = 1
        session['protocol_request'] += 1

        obj['version'] = 1

        msg = bytes(json.dumps(obj, separators=(',', ':')), 'utf-8')
        try:
            self._conn.sendall(msg)
        except:
            self._logger.exception("Exception encountered sending data to server")
            raise

        self._logger.debug("Message sent completely")

    def _recv_msg(self):
        """Receive JSON message from remote server"""

        data = bytes()
        obj = None

        try:
            data += self._conn.recv(4096)
            self._logger.debug('Raw messge received: "%s"', data)
            obj = json.loads(data)
        except json.JSONDecodeError as exp_obj:
            self._logger.error("Unable to parse received data.")
            self._logger.error("Cause: %s", exp_obj.msg)
            self._logger.error("Data: %s", exp_obj.doc)
            raise ProtocolException("Failed to parse received data")
        except:
            self._logger.exception("Exception encountered receiving data")
            raise

        missing_keys = [k for k in ['request', 'version', 'function'] if k not in obj]
        self._logger.debug("Missing keys: %s", missing_keys)
        if missing_keys:
            raise KeyError(
                "The following fields are missing from the received message: %s" %
                (missing_keys)
            )

        # Check for expected response
        if obj['request'] != self._expect_request:
            self._logger.error(
                "Unexpected request value %s. Expected: %s",
                obj['request'], self._expect_request)
            raise ProtocolException('Received unexpected request value')
        self._logger.debug("Request valid")

        if obj['version'] != 1:
            self._logger.debug("Message has expected request value")
            self._logger.error("Unexpected version %s.",
                               obj['version'])
            raise ProtocolException('Received unexpected version')
        self._logger.debug("Version valid")

        # Check for error
        try:
            err = obj['error']
            self._logger.debug("'error' field in received message. Value: %s", err)
            raise ProtocolException(err)

        except KeyError:
            self._logger.debug("'error' field not in received message")

        self._logger.debug("Function processed by server")
        return obj

    def new_actor(self, username, password):
        """Implement new-actor function"""
        try:
            obj = {
                'function': 'new-actor',
                'username': username,
                'password': password
            }

            self._logger.debug("Sending new-identity request to server")
            self._send_msg(obj)
            resp = self._recv_msg()

            return resp['session']
        except KeyError as obj:
            if 'session' in str(obj):
                self._logger.error("'session' field missing from response")
                raise ProtocolException("Unexpected response from server")

            self._logger.exception("Exception not for missing 'session' field")
            raise

    def login(self, username, password):
        """Implement login function"""

        ret_val = None
        try:
            obj = {
                'function': 'login',
                'username': username,
                'password': password
            }

            self._logger.debug("Sending login request to server")
            self._send_msg(obj)
            resp = self._recv_msg()
            ret_val = resp['session']
        except ProtocolException as err_obj:
            self._logger.debug("Contents of args: %s", err_obj.args)
            if str(err_obj) == 'authentication failed':
                self._logger.info("Invalid credentials")
            else:
                self._logger.error("Server returned an error: %s", err_obj)
                raise
        except KeyError as err_obj:
            if 'session' in str(err_obj):
                self._logger.error("'session' field missing from response")
                raise ProtocolException("Unexpected response from server")

            self._logger.exception("Exception not for missing 'session' field")
            raise

        return ret_val

    def check_session(self, sess_id, actor):
        """Implement check-session function"""

        ret_val = False
        try:
            obj = {}
            obj['function'] = 'check-session'
            obj['session'] = sess_id
            obj['actor'] = actor

            self._send_msg(obj)
            resp = self._recv_msg()
            ret_val = (resp['session'] == sess_id)
        except ProtocolException as err_obj:
            self._logger.debug("Contents of args: %s", err_obj.args)
            if str(err_obj) == 'invalid':
                self._logger.info("Invalid session")
            else:
                self._logger.error("Server returned an error: %s", err_obj)
                raise
        except KeyError as err_obj:
            if 'session' in str(err_obj):
                msg = "'session' field missing from response"
                self._logger.error(msg)
                raise ProtocolException(msg)

            self._logger.exception("Exception not from missing 'session' field")
            raise

        return ret_val

    def logout(self, sess_id):
        """Implement logout function"""

        try:
            obj = {}
            obj['function'] = 'logout'
            obj['session'] = sess_id

            self._send_msg(obj)
            self._recv_msg()
        except ProtocolException:
            self._logger.warning('ProtocolException encountered during logout')
        except BaseException: # pylint: disable=broad-except
            self._logger.exception('Exception encountered during logout')


# vim: set expandtab:ts=4:sw=4
