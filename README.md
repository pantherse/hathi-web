*2019-03-01:*  
*We, the Hathi authors,  are continuing this project and claim copyright to the changes that we will be making henceforth.*  
*We will be continuing to publish under the Apache open source license.*  
*2019-02-28:*  
*As our sponsoring organisation has decided to spin down, it has been agreed to public-domain the Hathi project.*  
*Code created on or before this date is free for everyone to use and/or claim.*  

# Hathi Web client

The Hathi-social web client

## Requirements

* [Python 3](https://www.python.org/)
* [pytest](https://docs.pytest.org/en/latest/)
* [pytest-catchlog](https://github.com/eisensheng/pytest-catchlog)
* [Flask](flask.pocoo.org)
* [Flask-Login](https://flask-login.readthedocs.io/en/latest/)
* [Activipy](https://github.com/w3c-social/activipy)
* [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup/)
* [html5lib parser](https://github.com/html5lib/)

## Quickstart
1. Clone this repo
1. Clone and run [hathi-server](https://gitlab.com/hathi-social/hathi-server)
1. Start hathi-server (check its README.md file)
1. `pip3 install flask activipy flask-login beautifulsoup4`
1. `flask run`
1. On a browser navigate to http://localhost:5000

## Configuration
The default configuration is in hathi/config/server.cfg and the logging config is logging.cfg in the same directory. The SECRET_KEY member in server.cfg must be filled in with some data; see http://flask.pocoo.org/docs/1.0/quickstart/#sessions for details.

To use a different log for development, set HATHI_CONFIG environment variable to the full path of the config file to use before starting the web app.

## Quick testing
This is instructions to view the actor's inbox, outbox, following, and followers collections

1. Run hathi-mock.py -s (Note: The server _must_ listen on localhost)
1. On another console run the following
   1. Run `ls -l tests/payload/testhathidb.json`
   1. Note the file size
   1. Run `curl -H 'Content-type: application/json' -H 'Content-Length: <file size> --data-binary '@tests/payload/testhathidb.json' http://localhost:8080/loaddb`
1. Start hathi-web
1. On your browser navigate to http://<server>:5000/
1. Enter the following in the appropriate fields:
   username = testing
   password = 123456

## Running unit test
To run the hathi-web unit test simply run "pytest" on the top-level directory
