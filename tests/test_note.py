# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

import logging
import sys

import flask
import json
import requests
from werkzeug.datastructures import MultiDict

from bs4 import BeautifulSoup

from activipy import vocab

import hathi
from hathi import note


class TestCollectNote(object):
    def test_add_members_present(self, client, caplog):
        """Test where all expected Note members are present """

        summary, content = note.collect_note(vocab.Note(
            "http://localhost:8080/1",
            summary={"en": "User One"},
            content={"en": "User One content"}
        ))
        assert summary == "User One"
        assert content == "User One content"

    def test_content_only(self, client, caplog):
        """ Test summary member is missing"""

        summary, content = note.collect_note(vocab.Note(
            "http://localhost:8080/2",
            content={"en": "User Two content"}
        ))
        assert content == "User Two content"
        assert summary is None

    def test_summary_only(self, client, caplog):
        """Test content member is missing"""

        summary, content = note.collect_note(vocab.Note(
            "http://localhost:8080/3",
            summary={"en": "User Three summary"}
        ))
        assert summary == "User Three summary"
        assert content is None

    def test_non_en(self, client, caplog):
        """Test where English text is not available"""

        summary, content = note.collect_note(vocab.Note(
            "http://localhost:8080/4",
            summary={"fr": "Should be a French-language summary"},
            content={"fr": "Should be a French-language content"}
        ))
        assert summary == "Should be a French-language summary"
        assert content == "Should be a French-language content"

    def test_all_string(self, client, caplog):
        """Test where content doesn't specify language"""

        summary, content = note.collect_note(vocab.Note(
            "http://localhost:8080/4",
            summary="Should be a plain summary",
            content="Should be a plain content"
        ))
        assert summary == "Should be a plain summary"
        assert content == "Should be a plain content"


class TestBuildNoteObj(object):
    def test_all_fields(self, caplog):
        """Test _build_note_obj"""

        obj = note._build_note_obj(
            "Content text", "Summary text", ("user1", "user2"),
            ("cc1", "cc2"), ("bcc1", "bcc2"), None
        )
        assert "Note" in obj.types
        assert obj["summary"] == "Summary text"
        assert obj["content"] == "Content text"

        to = obj["to"]
        assert len(to) == 2
        assert all(k in to for k in ("user1", "user2"))

        cc = obj["cc"]
        assert len(cc) == 2
        assert all(k in cc for k in ("cc1", "cc2"))

        bcc = obj["bcc"]
        assert len(bcc) == 2
        assert all(k in bcc for k in ("bcc1", "bcc2"))

    def test_content_only(self, caplog):

        obj = note._build_note_obj(
            "Content text", None, None, None, None, None
        )
        assert obj["content"] == "Content text"

        with pytest.raises(KeyError):
            obj["summary"]

        with pytest.raises(KeyError):
            obj["to"]

        with pytest.raises(KeyError):
            obj["cc"]

        with pytest.raises(KeyError):
            obj["bcc"]

    def test_to_only(self, caplog):

        obj = note._build_note_obj(
            "Content text", None, "user1", None, None, None
        )
        assert obj["content"] == "Content text"

        with pytest.raises(KeyError):
            obj["summary"]

        with pytest.raises(KeyError):
            obj["cc"]

        with pytest.raises(KeyError):
            obj["bcc"]

    def test_cc_only(self, caplog):

        obj = note._build_note_obj(
            "Content text", None, None, "user1", None, None
        )

        assert obj["content"] == "Content text"

        with pytest.raises(KeyError):
            obj["summary"]

        with pytest.raises(KeyError):
            obj["to"]

        with pytest.raises(KeyError):
            obj["bcc"]

    def test_bcc_only(self, caplog):

        obj = note._build_note_obj(
            "Content text", None, None, None, "user1", None
        )
        assert obj["content"] == "Content text"

        with pytest.raises(KeyError):
            obj["summary"]

        with pytest.raises(KeyError):
            obj["to"]

        with pytest.raises(KeyError):
            obj["cc"]


class TestGetNewNoteId(object):
    def test_note_good_obj(self, caplog, monkeypatch):
        """ Test UID is a create object that contains a UID of a Note """

        def mock_get_obj(url):
            obj = None
            if url == "http://localhost:8080/0":
                obj = vocab.Create(url,
                                   **{"object": "http://localhost:8080/1"})
            elif url == "http://localhost:8080/1":
                obj = vocab.Note(url, summary="Test Note",
                                 content="Test Content")
            else:
                raise RuntimeError("Invalid test URL")

            return obj

        monkeypatch.setattr(hathi.utils, "getObject", mock_get_obj)
        note_id = note._get_new_note_id("http://localhost:8080/0")
        assert note_id == "http://localhost:8080/1"

    def test_note_not_create(self, caplog, monkeypatch):
        """ Test where UID is not a create object """

        def mock_get_obj(url):
            return vocab.Add(
                url,
                **{"object": vocab.Note(
                        "http://localhost:8080/0", content="blah")})

        monkeypatch.setattr(hathi.utils, "getObject", mock_get_obj)
        with pytest.raises(RuntimeError) as err:
            note._get_new_note_id("http://localhost:8080/0")
            assert(
                "\"http://localhost:8080/0\" is not a \"Create\" object"
                in err.attrs
            )

    def test_uid_not_create(self, caplog, monkeypatch):
        """ Test where UID is not a create object """

        def mock_get_obj(url):
            return vocab.Add(
                url,
                **{"object": vocab.Note(
                        "http://localhost:8080/0", content="blah")})

        monkeypatch.setattr(hathi.utils, "getObject", mock_get_obj)
        with pytest.raises(RuntimeError) as err:
            note._get_new_note_id("http://localhost:8080/0")
            assert (
                "\"http://localhost:8080/0\" is not a \"Create\" object"
                in err.attrs
            )

    def test_not_note_create_obj(self, caplog, monkeypatch):
        """ Test UID is a create object is not a Note """

        def mock_get_obj(url):
            obj = None
            if url == "http://localhost:8080/0":
                obj = vocab.Create(url,
                                   **{"object": "http://localhost:8080/1"})
            elif url == "http://localhost:8080/1":
                obj = vocab.Event(url, summary="Test Note",
                                  name="Test Content")
            else:
                raise RuntimeError("Invalid test URL")

            return obj

        monkeypatch.setattr(hathi.utils, "getObject", mock_get_obj)
        with pytest.raises(AssertionError) as err:
            note._get_new_note_id("http://localhost:8080/0")

    def test_note_get_fail(self, caplog, monkeypatch):
        """ Test UID is a create object can't be retrieved """

        def mock_get_obj(url):
            obj = None
            if url == "http://localhost:8080/0":
                obj = vocab.Create(url,
                                   **{"object": "http://localhost:8080/1"})
            elif url == "http://localhost:8080/1":
                raise RuntimeError
            else:
                raise RuntimeError("Invalid test URL")

            return obj

        monkeypatch.setattr(hathi.utils, "getObject", mock_get_obj)
        with pytest.raises(RuntimeError) as err:
            note._get_new_note_id("http://localhost:8080/0")


class TestProcessNewNote(object):
    def test_missing_keys(self, caplog, monkeypatch):
        """Test required keys missing in _process_new_note params"""

        def mock_post_obj(url, content):
            """ Fail on call to utils.postObject
            """
            assert False

        monkeypatch.setattr(hathi.utils, 'postObject', mock_post_obj)

        with pytest.raises(KeyError) as excInfo:
            note._process_new_note(MultiDict({"content": "Test content"}), "")
            assert all(k in ("to", "cc", "bcc") for k in excInfo.args)

        with pytest.raises(KeyError) as excInfo:
            note._process_new_note(MultiDict({"summary": "Summary text"}), "")
            assert all(
                k in ("content", "to", "cc", "bcc") for k in excInfo.args)

        with pytest.raises(KeyError) as excInfo:
            note._process_new_note(MultiDict({"summary": "Summary text",
                                              "content": "Content text"}), "")
            assert all(k in ("to", "cc", "bcc") for k in excInfo.args)

    def test_keys_present(self, caplog, monkeypatch):
        """Test all required keys present in _process_new_note params"""

        user_id = "http://localhost:8080/unittest"

        def mock_post_obj(url, content):
            assert url == "%s/outbox" % (user_id)
            assert "Note" in content.types
            assert content["summary"] == "Summary text"
            assert content["content"] == "Content text"
            assert content["to"] == ["user1"]

            return "http://localhost:8080/0"

        def mock_get_obj(url):
            if url == "http://localhost:8080/0":
                return vocab.Create(
                    "http://localhost:8080/0",
                    **{"object": "http://localhost:8080/1"})
            elif url == "http://localhost:8080/1":
                return vocab.Note(
                    "http://localhost:8080/1",
                    summary="Test summary", content="Test Content")

        monkeypatch.setattr(hathi.utils, "postObject", mock_post_obj)
        monkeypatch.setattr(hathi.utils, "getObject", mock_get_obj)
        uid = note._process_new_note(
            MultiDict({
                "summary": "Summary text", "content": "Content text",
                "to": "user1"
            }),
            "%s/outbox" % (user_id))
        assert uid == "http://localhost:8080/1"


class TestBuildForm(object):
    def test_empty_form(self, app):
        """ Test the \"new note\" form
        """

        with app.test_request_context("/note/new"):
            form = note._build_note_form()
            page = BeautifulSoup(form, "html5lib")
            for i in ("to", "cc", "bcc", "summary"):
                assert 1 == len(page.find_all("input", {"name": i}))
            assert page.find_all("textarea", {"name": "content"})

    def test_valid_fail(self, app):
        """ Test that input fiels are marked with is-invalid
        """
        with app.test_request_context("/note/save"):
            flagged_fields = ["to", "cc", "bcc"]
            page = BeautifulSoup(
                note._build_note_form(valid_fail=flagged_fields),
                "html5lib"
            )
            for i in flagged_fields:
                field = page.find_all("input", {"name": i})
                assert len(field) == 1
                assert "is-invalid" in field[0]["class"]

    def test_rebuild_rcpt_fields(self, app):
        """ Test that "to" values are placed in "to" input fields
        on the form
        """

        with app.test_request_context("/note/save"):
            to_list = ["http://example.com/user1", "http://example.com/user2"]
            cc_list = ["http://example.com/cc1", "http://example.com/cc2"]
            bcc_list = ["http://example.com/bcc1", "http://example.com/bcc2"]
            page = BeautifulSoup(
                note._build_note_form(valid_fail=["content"],
                                      to_list=to_list,
                                      cc_list=cc_list,
                                      bcc_list=bcc_list),
                "html5lib"
            )
            field = page.find_all("textarea", {"name": "content"})
            assert "is-invalid" in field[0]["class"]
            field = page.find_all("input", {"name": "to"})
            assert len(field) == 2
            assert any(True for f in field if f["value"] == to_list[0])
            assert any(True for f in field if f["value"] == to_list[1])

            field = page.find_all("input", {"name": "cc"})
            assert len(field) == 2
            assert any(True for f in field if f["value"] == cc_list[0])
            assert any(True for f in field if f["value"] == cc_list[1])

            field = page.find_all("input", {"name": "bcc"})
            assert len(field) == 2
            assert any(True for f in field if f["value"] == bcc_list[0])
            assert any(True for f in field if f["value"] == bcc_list[1])

        with app.test_request_context("/note/save"):
            page = BeautifulSoup(
                note._build_note_form(valid_fail=["to", "cc", "bcc"]),
                "html5lib"
            )
            field = page.find_all("textarea", {"name": "content"})
            assert len(field) == 1

            field = page.find_all("input", {"name": "summary"})
            assert len(field) == 1

            for i in ["to", "cc", "bcc"]:
                field = page.find_all("input", {"name": i})
                assert len(field) == 1
                assert "is-invalid" in field[0]["class"]

    def test_prepop_content_field(self, app):
        """ Test that "to" values are placed in "to" input fields
        on the form
        """

        with app.test_request_context("/note/save"):
            page = BeautifulSoup(
                note._build_note_form(summary="Test summary",
                                      content="Test\n\nContent"),
                "html5lib"
            )
            field = page.find_all("textarea", {"name": "content"})
            assert len(field) == 1
            assert field[0].contents[0] == "Test\n\nContent"

            field = page.find_all("input", {"name": "summary"})
            assert len(field) == 1
            assert field[0]["value"] == "Test summary"


# vim: set expandtab:ts=4:sw=4
